# Sym_stepup_DCDC

Symmetric step up DCDC converter

# Goals 

- keep it cheap

- use easy to find components _(even recycling boards)_

- keep it quite small _(using dip-8 components)_

# Use case

Many AOP or comparators do not need a high symetric voltage, then simple step-up can be used.

# Images

![PCB_components](./Stepup_dcdc.png)

![PCB_copper](./Stepup_dcdc_copper.png)

![Schema](./schema.png)

# Datasheet

[MAX680](./MAX680-MAX681.pdf)

# Workfiles

[kicad](./kicad/)

# build

[Gerbers](./build/)

# LICENSE

[license](./LICENSE)
